/*
//	Copyright Marcus O'Brien 2016-2017
//
//	All code and algorithms not from existing libraries and code, belong to Macus O'Brien
//	Please contact Marcus O'Brien at the following to use or modify this code :
//		marcusobrien@yahoo.com
//
//	Official web site is :
//		http://www.roboticsfordreamers.com

*/

//This is a super simple test for the board, it sends some basic text to the serial monitor from the 101 
//board to show it is working. Only turn this on to make sure the board is talking to your PC via the SMon.
//#define TEST_BOARD 

// NOTE LABEL 0 in data file is CATEGORY 1 in the NN - Can not use Cat 0, its reserved for a particular function in the hardware level, 
// so the OCR image of 0, or label 0, is stored as cat 1
// Ergo, all OCR types are stored as NN cat = label + 1, ie an image of the number 4 has a label of 4 in the label file, and is stored as cat 5 in the NN

#include <SD.h>
#include <CuriePME.h>
#include <CurieTime.h>

//Version of this sketch source file
//I use this when working to make sure I have the correct version of the sketch
//running on the Arduino. So when I make a change to this source code file, I also up this version number
//Then when I upload the code to the 101, I make sure it has the right version on it
static const char* VERSION = "1.3 Official";

//We can test the NN with either the training data ot the test data
//We use the training data to see how good it is classifying images, the reason is
//that it should know the training data 100% - as this the data it actually used to classify the images!
//The test data has never been shown to the NN before (as it has only been shown the training data), so 
//the test data is an indication of how good the NN has learn the general patterns of the characters

//This tells us whether we are testing with the training data or the test data
enum CurrentTestingData
{
	Training,
	Test
} CurrentTestingDataState;


//Used for SD card config on the Arduino - it it the Pin used to talk to the SD
static const int chipSelect = 10;

//We use this when training the NN
//we will iterate over ALL the training image data until there are no new committed 
//neurons on the training run, so keep learning until no new neurons are committed, ie 
//the NN is not changing anymore
static int ITERATE_LEARN_UNTIL_NO_NEW_COMMITTED = true;

//If we are using ITERATE_LEARN_UNTIL_NO_NEW_COMMITTED , then we can set a max number - so learn again
//until no new committed neurons up to a maximum X, otherwise could go on for ever if each run results 
//newly committed neurons ?? - This is just a saftey cut off, make sure we do actually finish training
//should never happen though
#define MAX_ITERATES_LEARN_UNTIL_NO_NEW_COMMITTED 5

//We save the number of newly committed neurons per run, so each int in the array is the numbr 
//of new nuerons that were committed for that run, there is a max of 5 runs, so there will be up
//to 5 ints, each one telling us how mnay new neurons got used.
static int NewCommittedPerTrain[MAX_ITERATES_LEARN_UNTIL_NO_NEW_COMMITTED];

// - this is the index for the above array, we set this when training so we know
//how mnay times we went over the training data - ie we go over the data every time we 
//have learnt new neurons ie the NN has changed  
static int NewCommittedPerTrain_Index = 0;

//This tells us how many patterns were learnt - sometimes we run out as the number of commmitted gets 
//to 128 before we have finished training, so we might have trained 128 patterns, and every time we trained
//a new neuron was committed, so we stop, as we dont have more neurons left to use - we used the full 128. 
//Now we need to remember this, as when testing with the training data - we want to make sure we get 100% 
//correct, BUT not for images that we didn't learn.
static int NumberOfImagesUsedInTraining = 0;

//Turn this on means that we only test using the images we used in training - the number above
//as this is not always the MAX to test, as if we run out of neurons due to all committed then
//we stop training
static bool UseOnlyImagesFromTraining = true;

//This flag tells us we are writing or reading data, certain registers on the chip are used during this process
//so its best not to do certain things in the middle of it. I use this flag to tell me Im doing something 
//with these registers
static bool ITERATING_NEURON_NETWORK = false; //DO NOT CHANGE THIS!!

//This is for printing the results, true if they have wrapped around, so it should always start with false
//affects print results only
static bool WrappedAroundResults = false; //DO NOT CHANGE THIS!!

//Only learn the first N images ie 4 means learn OCR image types 0 to 3, 10 is all of the image types - 
//all of the characters from 0 through to 9
static int numberOCRImageTypesToLearn = 10; // All types 0-9

//Number of images to train with - so when I get to this number of images stop learning
int numberImagesToTrain = 5000;

//Number of images to test - when we have tested this number of images stop testing
int NUM_TO_TEST = 3000;

//Use this to only test images if we know that category. So if we learn only some categories (dont forget a cat
//equivalent to an image type), so if we only learn 0-4 image types then only test those types of images eg 0-4 when
//doing a test run, so this stops us testing images from the test data if the NN hasnt learned those image types
//If we do test unknown cats or image types the results will be far worse !
static bool RESTRICT_TESTING_TO_KNOWN_CATS = true;

//Serial Manager comms speed - Make sure you set your SM baud rate to 9600 !
static const int SerialBAUD = 9600;

//We retain a list of user commands to see what happened - the user command history
static const int MAX_COMMANDS_HISTORY_LEN = 30;
int nextCommand = 0;
char COMMANDS_LIST[MAX_COMMANDS_HISTORY_LEN];

//When timing an operation across functions we store the start time in here in seconds
//this is normally current unix time secs ie now
static unsigned long GlobalStartTimerSeconds = 0;

//We use this when training to iterate over the same image individually N times
#define NUMBER_ITERATIONS_LEARN_SAME_IMAGE 1

#define INITIAL_CATEGORY 99
#define NETWORK_CONTEXT 10
#define NN_FORGET 500
#define LED_OUT 13
#define MAX_NEURONS Intel_PMT::maxNeurons

#define BLINK_RATE 1000
#define NUM_IMAGES_TYPES 10

//Just to check we dont put in a silly value for number of ocr images to train from 
//the data, this is the max number the program will try to learn
#define MAX_NUM_IMAGES_TO_LEARN_EVER 10000

File *dataFile = NULL;
File *labelFile = NULL;

//Original NMist image in data file dimensions
static const int ORIG_ROW = 28;
static const int ORIG_COL = 28;
static const int LEN = ORIG_ROW * ORIG_COL;

//File names on Sd card - test and training data, and corresponding label files
static const char *TestImagesDataFileFN = "TESTI.DAT";
static const char *TestLabelsFileFN = "TESTL.DAT";
static const char *TrainImagesDataFileFN = "TRAINI.DAT";
static const char *TrainLabelsFileFN = "TRAINL.DAT";

//Some pre-processor directives for verbose execution - useful as they provide debug output to serial monitor
//#define DEBUG
//#define PROCESS_FILE_DEBUG
#define DEBUG_LEARNING

//Outputs more info when NN is tying to guess the label type or cat
#define DEBUG_CLASSIFY

//Used for teh image after we have processed it
static const int32_t PROCESSED_IMAGE_SIZE = LEN / 8;
uint8_t* ProcessedImage = new uint8_t[PROCESSED_IMAGE_SIZE];

//Image is read into here from file, then processed - as its too large for teh hardware NN vector of 128 bytes
uint8_t* RawImage = new uint8_t[LEN];

//This is a list of the known categories in the NN
int *knownCats = new int[NUM_IMAGES_TYPES];

//KNN Mode uses most 1 neruon per cat - rubbish

//Initial settings - passed to begin in the NN
Intel_PMT::PATTERN_MATCHING_DISTANCE_MODE startMode = Intel_PMT::PATTERN_MATCHING_DISTANCE_MODE::LSUP_Distance;
Intel_PMT::PATTERN_MATCHING_CLASSIFICATION_MODE startClass = Intel_PMT::PATTERN_MATCHING_CLASSIFICATION_MODE::RBF_Mode;
#define CUR_MIN_AIF 1
#define CUR_MAX_AIF 50000

struct result
{
	int LastCorrect = 0;
	int LastTested = 0;
	Intel_PMT::PATTERN_MATCHING_DISTANCE_MODE distanceMode = Intel_PMT::PATTERN_MATCHING_DISTANCE_MODE::L1_Distance;
	Intel_PMT::PATTERN_MATCHING_CLASSIFICATION_MODE classMode = Intel_PMT::PATTERN_MATCHING_CLASSIFICATION_MODE::KNN_Mode;

	void setResult(int lc, int lt,
		Intel_PMT::PATTERN_MATCHING_DISTANCE_MODE  distMode,
		Intel_PMT::PATTERN_MATCHING_CLASSIFICATION_MODE clMode)
	{
		LastCorrect = lc;
		LastTested = lt;
		distanceMode = distMode;
		classMode = clMode;
	}

	void print(int num)
	{
		Serial.print("Testing Result of run ");
		Serial.println(num);
		Serial.print(" - Got ");
		Serial.print(LastCorrect);
		Serial.print(" correct out of ");
		Serial.print(LastTested);
		Serial.print(" images");
		
		if (distanceMode == Intel_PMT::PATTERN_MATCHING_DISTANCE_MODE::L1_Distance)
		{
			Serial.print(" - Distance mode: L1 ");
		}
		else
		{
			Serial.print(" - Distance mode: LSUP ");
		}

		Serial.print(" Classification mode: ");
		if (classMode == Intel_PMT::PATTERN_MATCHING_CLASSIFICATION_MODE::RBF_Mode)
		{
			Serial.println(" RBF");
		}
		else
		{
			Serial.println(" KNN");
		}
	}
};

//Results from previous runs
static const int NUMBER_PREVIOUS_RESULTS = 20;
result RESULTS[NUMBER_PREVIOUS_RESULTS];
static int CurrentRESULTIndex = 0;

void checkSDCard()
{
#ifdef AVR
	Serial.print("hello its AVR !!!!!");
#endif

	Serial.println("Check SD");

	Sd2Card card;
	SdVolume volume;
	SdFile root;

	if (!card.init(SPI_HALF_SPEED, chipSelect))
	{
		Serial.println("SD Init failed. Check:");
		Serial.println("* SD is inserted?");
		Serial.println("* Wiring correct?");
		Serial.println("* chipSelect in code - must match shield-module?");
		return;
	}
	else
	{
		Serial.println("SD card GOOD-wiring is correct and a card is present.");
	}

	Serial.print("Card type:");
	switch (card.type())
	{
	case SD_CARD_TYPE_SD1:
		Serial.println("SD1");
		break;
	case SD_CARD_TYPE_SD2:
		Serial.println("SD2");
		break;
	case SD_CARD_TYPE_SDHC:
		Serial.println("SDHC");
		break;
	default:
		Serial.println("Unknown");
	}

	if (!volume.init(card))
	{
		Serial.println("Could not find FAT16 or FAT32 partition.\nIs card formatted");
		return;
	}

	uint32_t volumesize;
	Serial.print("Volume is FAT");
	Serial.println(volume.fatType(), DEC);
	Serial.println();

	volumesize = volume.blocksPerCluster();    // clusters are collections of blocks
	volumesize *= volume.clusterCount();       // we'll have a lot of clusters
	volumesize *= 512;                            // SD card blocks are always 512 bytes
	Serial.print("Volume size (bytes): ");
	Serial.println(volumesize);
	Serial.print("Volume size (Kbytes): ");
	volumesize /= 1024;
	Serial.println(volumesize);
	Serial.print("Volume size (Mbytes): ");
	volumesize /= 1024;
	Serial.println(volumesize);

	Serial.println("Files (name, date and size in bytes): ");
	root.openRoot(volume);

	root.ls(LS_R | LS_DATE | LS_SIZE);

	Serial.println("Checking files");

	if (SD.exists(TrainImagesDataFileFN) == true)
	{
		Serial.println("Found the train data file.");
	}
	else
	{
		Serial.println("Could NOT find the training data file !!");
	}

	if (SD.exists(TrainLabelsFileFN) == true)
	{
		Serial.println("Found the train labels file.");
	}
	else
	{
		Serial.println("Could NOT find the train labels file !!");
	}

	if (SD.exists(TestImagesDataFileFN) == true)
	{
		Serial.println("Found the test data file.");
	}
	else
	{
		Serial.println("Could NOT find the test data file !!");
	}

	if (SD.exists(TestLabelsFileFN) == true)
	{
		Serial.println("Found the test labels file.");
	}
	else
	{
		Serial.println("Could NOT find the test labels file !!");
	}

	SD.begin(chipSelect);
}

void startCurieNN()
{
	Serial.println("Resetting NN Pre Reset State :");
	NNInfo();
	CuriePME.begin(NETWORK_CONTEXT, startMode, startClass, CUR_MIN_AIF, CUR_MAX_AIF);
	Serial.println("Resetting NN POST Reset State :");
	NNInfo();
}

void setupTest()
{
	Serial.begin(SerialBAUD);
	Serial.println("Hello");
}

void loopTest()
{
	Serial.println("101 Board is ok.");
}


void loop()
{
#ifdef TEST_BOARD
	loopTest();
#else
	loopProper();
#endif
}

void setup()
{
#ifdef TEST_BOARD
	setupTest();
#else
	setupProper();
#endif
}

void setupProper()
{
	numberOCRImageTypesToLearn = constrain(numberOCRImageTypesToLearn, 0, NUM_IMAGES_TYPES);
	numberImagesToTrain = constrain(numberImagesToTrain, 0, MAX_NUM_IMAGES_TO_LEARN_EVER);

	pinMode(chipSelect, OUTPUT);
	digitalWrite(chipSelect, HIGH);

	Serial.begin(SerialBAUD);
	Serial.println("Hello....");
	while (!Serial);

	Serial.println("Hello....");

	delay(500);

	startCurieNN(); 	
	
	SD.begin(chipSelect);

	setNNOptions();
}

void clearNN()
{
	Serial.println("Wipe NN");
	CuriePME.forget();
	setNNOptions();
}

void printTimeDifference(String operationName, unsigned long origTime)
{
	Serial.print("The ");
	Serial.print(operationName);
	Serial.print(" operation took ");
	unsigned long dif = now() - origTime;
	Serial.print(dif);
	Serial.println(" seconds to perform");
}

void markStartTimer()
{
	//This is the global timer when crossing over single funcitons
	GlobalStartTimerSeconds = now();
}

void printEndTime(String opName)
{
	printTimeDifference(opName, GlobalStartTimerSeconds);
	endTimer();
}

void endTimer()
{
	GlobalStartTimerSeconds = 0;
}

void toggleRestrictTesting()
{
	if (RESTRICT_TESTING_TO_KNOWN_CATS)
	{
		Serial.println("Disable testing known cats");
		RESTRICT_TESTING_TO_KNOWN_CATS = false;
	}
	else
	{
		Serial.println("Enable testing known cats");
		RESTRICT_TESTING_TO_KNOWN_CATS = true;
	}
}

void printTestOnlyWithTrainingImages()
{
	if (UseOnlyImagesFromTraining == true)
	{
		Serial.println("We will only use the exact images used in training to test the NN.");
		if (NumberOfImagesUsedInTraining > 0)
		{
			Serial.print("We used ");
			Serial.print(NumberOfImagesUsedInTraining);
			Serial.println(" images when training, so we will only use this number when testing with the training data.");
		}
	}
	else
	{
		Serial.println("We will use all the images in the training data to test the NN.");
	}
}

void toggleNNType()
{
	Serial.println("Trying to set mode to ");
	if (CuriePME.getClassifierMode() == Intel_PMT::PATTERN_MATCHING_CLASSIFICATION_MODE::RBF_Mode)
	{
		Serial.println("KNN - will NOT iterate until committed neurons is zero per train run");
		CuriePME.setClassifierMode(Intel_PMT::PATTERN_MATCHING_CLASSIFICATION_MODE::KNN_Mode);
		//If we remove line below the training gets done 1 time, otherwise twice, does it make a difference in KNN ? TODO
		ITERATE_LEARN_UNTIL_NO_NEW_COMMITTED = false;
	}
	else
	{
		Serial.println("RBF");
		Serial.println("WILL now iterate until committed neurons is zero per train run");
		CuriePME.setClassifierMode(Intel_PMT::PATTERN_MATCHING_CLASSIFICATION_MODE::RBF_Mode);
		ITERATE_LEARN_UNTIL_NO_NEW_COMMITTED = true;
	}
	Serial.println("Sanity check - NN Mode is now ");
	printNNMode();
}

void setNNOptions()
{
	Serial.println("Setting the NN options");
	Serial.print("Current Global Context:");
	Serial.println(CuriePME.getGlobalContext());
}

void printDistanceMode()
{
	if (CuriePME.getDistanceMode() == Intel_PMT::PATTERN_MATCHING_DISTANCE_MODE::L1_Distance)
	{
		Serial.println("Current Distance mode is L1");
	}
	else
	{
		Serial.println("Current Distance mode is LSUP");
	}
}

void toggleDistanceMode()
{
	printDistanceMode();

	if (CuriePME.getDistanceMode() == Intel_PMT::PATTERN_MATCHING_DISTANCE_MODE::L1_Distance)
	{
		CuriePME.setDistanceMode(Intel_PMT::PATTERN_MATCHING_DISTANCE_MODE::LSUP_Distance);
		Serial.println("Set the NN to LSUP Distance");
	}
	else
	{
		CuriePME.setDistanceMode(Intel_PMT::PATTERN_MATCHING_DISTANCE_MODE::L1_Distance);
		Serial.println("Set the NN to L1 Distance");
	}

	printDistanceMode();
}

//Populate knownCats - the list of known categories in the NN, terminated with a 99
void whatCatsDoesNNKnow(bool printIt = false)
{
	if (checkIterator()) return;

	clearImagesArray(knownCats);

	int committedNuerons = CuriePME.getCommittedCount();

	Intel_PMT::neuronData data_array;
	int catIndex = 0;
	for (int i = 1; i < committedNuerons + 1; i++)
	{
		CuriePME.readNeuron(i, data_array);
		//if we dont have the cat in the array already
		if (intArrayContains(knownCats, NUM_IMAGES_TYPES, data_array.category) == false)
		{
			knownCats[catIndex] = data_array.category;
			++catIndex;
		}
	}

	if (printIt)
	{
		Serial.println("NN knows following cats : ");
		for (int i = 0; i < NUM_IMAGES_TYPES; i++)
		{
			if (knownCats[i] > 0)
			{
				Serial.print(knownCats[i]);
				Serial.print(", ");
			}
		}
	}
}

void printNNMode()
{
	if (CuriePME.getClassifierMode() == Intel_PMT::PATTERN_MATCHING_CLASSIFICATION_MODE::RBF_Mode)
	{
		Serial.println("NN Mode is set to RBF");
	}
	else
	{
		Serial.println("NN Mode is set to KNN");
	}
}

void printKnownCats()
{
	Serial.print("Known categories: ");

	whatCatsDoesNNKnow();

	for (int i = 0; (i < NUM_IMAGES_TYPES && knownCats[i] != 0); i++)
	{
		Serial.print(knownCats[i]);
		if ((i < (NUM_IMAGES_TYPES - 1)) && knownCats[i + 1] > 0)
			Serial.print(", ");
		else
			Serial.print("\n\n");
	}
}

void NNInfo()
{
	int neuronsCommitted = CuriePME.getCommittedCount();
	int maxNeurons = Intel_PMT::maxNeurons;
	int neuronSize = Intel_PMT::maxVectorSize;
	
	printIterationStatus();

	Serial.print("Neuron Vector Size="); Serial.println(neuronSize);
	Serial.print("Total Number of Neurons="); Serial.println(maxNeurons);
	Serial.print("Neurons committed="); Serial.println(neuronsCommitted);
	Serial.print("Neurons available="); Serial.println(maxNeurons - neuronsCommitted);
	Serial.print("Current Context="); Serial.println(CuriePME.getGlobalContext());
	Serial.print("Number Training Images="); Serial.println(numberImagesToTrain);
	Serial.print("OCR Types to learn from file:"); Serial.println(numberOCRImageTypesToLearn);
	Serial.print("Number of images to test :"); Serial.println(NUM_TO_TEST);
	Serial.print("Testing for only known cats ");

	if (RESTRICT_TESTING_TO_KNOWN_CATS)
		Serial.println("enabled");
	else
		Serial.println("disabled");
	
	printNNMode();

	printDistanceMode();

	printTestOnlyWithTrainingImages();

	if (NumberOfImagesUsedInTraining > 0)
	{
		printNumberOfImagesInTraining();
		Serial.println("Last training run - newly committed neurons per iteration");
		for (int count = 0; count < NewCommittedPerTrain_Index; count++)
		{
			int newCom = NewCommittedPerTrain[count];
			Serial.print("Training Run ");
			Serial.print(count + 1);
			Serial.print(" added ");
			Serial.print(newCom);
			Serial.println(" more committed neurons");
			if (newCom == 0)
			{
				break;
			}
		}

		printKnownCats();
	}
	else
	{
		Serial.println("Havent trained network yet.");
	}

	Serial.print("Will store ");
	Serial.print((int)NUMBER_PREVIOUS_RESULTS);
	Serial.println(" classification run results");
}

void printIterationStatus()
{
	if (ITERATE_LEARN_UNTIL_NO_NEW_COMMITTED)
	{
		Serial.print("Learning the patterns until no new committed neurons, due to learning is order dependent :-( A maximum of ");
		Serial.print(MAX_ITERATES_LEARN_UNTIL_NO_NEW_COMMITTED);
		Serial.println(" times");
	}
	else
	{
		Serial.println("Learning the data patterns once");
	}

	if (NUMBER_ITERATIONS_LEARN_SAME_IMAGE < 2)
	{
		Serial.println("Learning each indivdual pattern only once");
	}
	else
	{
		Serial.print("Learning each indivdual pattern ");
		Serial.print((int)NUMBER_ITERATIONS_LEARN_SAME_IMAGE);
		Serial.println(" times");
	}
}

void iterativeTraining()
{
	unsigned int stTime = now();

	printIterationStatus();

	//We save the number of newly committed neurons per run - this is the index
	NewCommittedPerTrain_Index = 0;

	//clear the array as well
	clearArray(NewCommittedPerTrain, MAX_ITERATES_LEARN_UNTIL_NO_NEW_COMMITTED);

	Serial.print("Training loop begun, 1st Iteration....");
	if (ITERATE_LEARN_UNTIL_NO_NEW_COMMITTED)
	{
		uint16_t iterations = 1;
		while (iterations<MAX_ITERATES_LEARN_UNTIL_NO_NEW_COMMITTED+1) 
		{
			int newlyCommittedNeurons = trainNetwork();
			++iterations;
			Serial.print("Training loop finished, just committed ");
			Serial.print(newlyCommittedNeurons);
			Serial.print(" more neurons.");
			
			int neuronsLeftToTrain = (Intel_PMT::maxNeurons -1) - CuriePME.getCommittedCount();

			Serial.print("There are ");
			Serial.print(neuronsLeftToTrain);
			Serial.println(" neurons left to train");
			if (neuronsLeftToTrain < 1)
			{
				Serial.println("Oops - no more neurons ! Stop Iterative Training");
				break;
			}
			if (newlyCommittedNeurons < 1)
			{
				Serial.println("Finished iterative testing as no new neurons were committed");
				break;
			}
			Serial.println("Now doing iteration #");
			Serial.println(iterations);
		}
	}
	else
	{
		trainNetwork();
	}
	printTimeDifference("Training", stTime);
}

void printNumberOfImagesInTraining()
{
	Serial.print("Training Used ");
	Serial.print(NumberOfImagesUsedInTraining);
	Serial.println(" images from the training data");
}

void resetNumberOfImagesInTraining()
{
	NumberOfImagesUsedInTraining = 0;
	Serial.println("Reset the number of images used in training");
}

void setModesToCurrent()
{
	startClass = CuriePME.getClassifierMode();
	startMode = CuriePME.getDistanceMode();
}

void trainNN()
{
	setModesToCurrent();
	startCurieNN();
	resetNumberOfImagesInTraining();
	Serial.println("Training the NN");
	iterativeTraining();
	Serial.println("Finished training the NN !!");
	printDistanceMode();
	printNumberOfImagesInTraining();
}

void openTestingDataFiles()
{
	SD.begin(chipSelect);

	dataFile = new File(SD.open(TestImagesDataFileFN));
	if (!dataFile)
	{
		Serial.print("Failed to open the testing data file:");
		Serial.println(TestImagesDataFileFN);
	}
	else
	{
		Serial.print("Opened the testing data file:");
		Serial.println(TestImagesDataFileFN);

		if (dataFile->available())
		{
			Serial.println("Testing data file available.");
		}
		else
		{
			Serial.println("Testing data file NOT available.");
		}
	}

	labelFile = new File(SD.open(TestLabelsFileFN));
	if (!labelFile)
	{
		Serial.print("Failed to open the testing labels file:");
		Serial.println(TestLabelsFileFN);
	}
	else
	{
		Serial.print("Opened the testing labels file:");
		Serial.println(TestLabelsFileFN);

		if (labelFile->available())
		{
			Serial.println("Testing label file available.");
		}
		else
		{
			Serial.println("Testing label file NOT available.");
		}
	}
}

void openTrainingDataFiles()
{
	SD.begin(chipSelect);

	dataFile = new File(SD.open(TrainImagesDataFileFN));
	if (!dataFile)
	{
		Serial.print("Failed to open the training data file:");
		Serial.println(TrainImagesDataFileFN);
		closeFiles();
		return;
	}
	else
	{
		Serial.print("Opened the training data file:");
		Serial.println(TrainImagesDataFileFN);

		if (dataFile->available())
		{
			Serial.println("Training data file available.");
		}
		else
		{
			Serial.println("Training data file NOT available.");
			closeFiles();
			return;
		}
	}

	labelFile = new File(SD.open(TrainLabelsFileFN));
	if (!labelFile)
	{
		Serial.print("Failed to open the training labels file:");
		Serial.println(TrainLabelsFileFN);
		closeFiles();
		return;
	}
	else
	{
		Serial.print("Opened the training labels file:");
		Serial.println(TrainLabelsFileFN);

		if (labelFile->available())
		{
			Serial.println("Training label file available.");
		}
		else
		{
			Serial.println("Training label file NOT available.");
			closeFiles();
			return;
		}
	}

}

void closeFiles()
{
	Serial.println("About to close data and label files.");
	if (dataFile)
	{
		dataFile->close();
		dataFile = NULL;
	}
	if (labelFile)
	{
		labelFile->close();
		labelFile = NULL;
	}
	Serial.println("Closed data and label files.");
}

void printImage(const char*  message, uint8_t* pattern, int len)
{
	Serial.println(message);
	Serial.print("Image is ");
	for (int index = 0; index < len; index++)
	{
		Serial.print(pattern[index]);
		Serial.print(" ");
	}
	Serial.println("  ");
}

void processImage()
{
  //Process the image to remove someof the data. The raw image is too large to fix inside a single vector. 
  //The data for the NN has to be a maximum of 128 bits. So lets use a pixel for each bit. This means
  //that we can only have a bit on or off, and so the pixel must be on or off. As the drawing of the OCR
  //character uses gray scale there are pixel values for each pixel from 0-255. But a character doesn't change 
  //due to its pixel colour, a character is defined by its shape. so lets change the COR image to a shape
  //and so, each pixel with a value over 2 is on, and each one below that is off
  
#ifdef PROCESS_FILE_DEBUG
	printImage("*********** Image is being converted from  : ", RawImage, LEN);
#endif

	//Anything above this pixel value is considered to be on, anything below or equal is off
	const uint8_t pixelThreshold = 2;

	//Monochrome the image
	for (int pixel = 0; pixel < LEN; pixel++)
	{
		if (RawImage[pixel] >= pixelThreshold)
			RawImage[pixel] = 1;
		else
			RawImage[pixel] = 0;
	}

	//We have 28x28 pixels of either on or off
	//so squash it into 98 bytes - so we have converted 784 to 98

	//Each pixel is now a bit, and therefore the new size of the array to pass in as a 
	//training character is LEN/8, as this is the number of new pixels !!

	int ProcessedImageIndex = 0;

	//Or the new byte with MSB first - bit 8 - left to right
	int BitOrValue = 128;
	ProcessedImage[0] = 0;

	for (int originalImageIndex = 0; originalImageIndex < LEN; originalImageIndex++)
	{
		uint8_t originalPixel = RawImage[originalImageIndex];

#ifdef PROCESS_FILE_DEBUG
		Serial.print("Original Pixel At (");
		Serial.print(originalImageIndex);
		Serial.print(") Was ");
		Serial.println(originalPixel);
#endif

		//The value we Bitwise OR with goes from 128 to 1, then round again
		if (BitOrValue > 1)
		{
			if (originalPixel == 1)
			{
#ifdef PROCESS_FILE_DEBUG
				Serial.print(" The originalpixel was ON so OR with ");
				Serial.print(BitOrValue);
#endif

				//or the pixel into the new one
				ProcessedImage[ProcessedImageIndex] = ProcessedImage[ProcessedImageIndex] | BitOrValue;
				//half the bitor value
				BitOrValue = BitOrValue >> 1;

#ifdef PROCESS_FILE_DEBUG
				Serial.print(" New Pixel[ ");
				Serial.print(ProcessedImageIndex);
				Serial.print("] is ");
				Serial.println(ProcessedImage[ProcessedImageIndex]);
#endif        
			}
			else if (originalPixel ==0)
			{
#ifdef PROCESS_FILE_DEBUG
				Serial.print("Original Pixel OFF - OR bit'ted with : "); Serial.print(BitOrValue);
#endif 
				//half the bitor value
				BitOrValue = BitOrValue >> 1;
#ifdef PROCESS_FILE_DEBUG
				Serial.print(" new OR bit Value : "); Serial.println(BitOrValue);
#endif 
			}
			else
			{
				Serial.println("IMAGE CONVERSION FAIL - not 1 or 0");
			}
		}
		else
		{

#ifdef PROCESS_FILE_DEBUG
			Serial.println("At last 8 block pixel !!");
#endif

			//Bit or value is 1 so this is the last original pixel for the new pixel
			//add one to the pixel if the old pixel is on
			if (originalPixel == 1)
			{
#ifdef PROCESS_FILE_DEBUG
				Serial.print("old pixel ON - New was pixel:");
				Serial.print(ProcessedImage[ProcessedImageIndex]);
				Serial.print(" is Now:");
				Serial.println(ProcessedImage[ProcessedImageIndex] + 1);
#endif
				ProcessedImage[ProcessedImageIndex] = ProcessedImage[ProcessedImageIndex] + 1;
			}
			else
			{
#ifdef PROCESS_FILE_DEBUG
				Serial.println("Original Pixel was OFF");
#endif 
			}

#ifdef PROCESS_FILE_DEBUG
			Serial.println("OR BIT value got to one ! Reset to 128 ");
#endif 

			//Reset the bit to or in the byte
			BitOrValue = 128;

			//move on to the next new byte
			++ProcessedImageIndex;

			ProcessedImage[ProcessedImageIndex] = 0;
#ifdef PROCESS_FILE_DEBUG
			Serial.println("We did 8 bits so on to next Pixel:" + ProcessedImageIndex);
#endif
		}
#ifdef PROCESS_FILE_DEBUG
		Serial.print("Currently the new pixel ["); Serial.print(ProcessedImageIndex); Serial.print("]="); Serial.println(ProcessedImage[ProcessedImageIndex]);
#endif
	}
#ifdef DEBUG
	printImage("*********** Image was converted from  : ", RawImage, LEN);
	printImage("*********** Image is : ", ProcessedImage, PROCESSED_IMAGE_SIZE);
#endif
}

int trainLabelSeekPos = 8;
int trainDataSeekPos = 16;

//Learns the data patterns
//Returns the number of newly committed neruons
uint16_t trainNetwork()
{
	openTrainingDataFiles();

	uint16_t startCommittedNeurons = CuriePME.getCommittedCount();

	int images_types[NUM_IMAGES_TYPES];
	clearImagesArray(images_types);

	if (!labelFile || !dataFile)
	{
		Serial.println("Training network failed - files not opened.");
		return 0;
	}

	if (labelFile->available() == 0)
	{
		Serial.println("Training network failed - label file not available.");
		return 0;
	}

	if (dataFile->available() == 0)
	{
		Serial.println("Training network failed - data file not available.");
		return 0;
	}

	int labelNum = 0;
	labelFile->seek(trainLabelSeekPos);
	dataFile->seek(trainDataSeekPos);

	int count_images_in_file = 0;

	//When we hit the number of types of image to learn, stop learning new types
	bool NoNewOCRTypes = false;

	while (count_images_in_file < numberImagesToTrain)
	{
		labelNum = labelFile->read();
		if (NoNewOCRTypes && images_types[labelNum] == 0)
		{
#ifdef DEBUG_LEARNING
			//if label is a new type skip this image
			Serial.print("Hit limit of new OCR types.OCR image type is new-type:"); 
			Serial.print(labelNum); 
			Serial.println(" skip");
#endif
		}
		else
		{
			count_images_in_file++;

			uint16_t imageCategory = labelNum + 1;
			++images_types[labelNum];

			for (int i = 0; i < LEN; i++)
			{
				RawImage[i] = dataFile->read();
			}
			processImage();

			//We keep a record of how many images we learnt, train is iterate, we dont want to keep incrementing
			//and we only want to increment if the count is bigger - this stops the count changing on subsequent iterations
			if (NumberOfImagesUsedInTraining < count_images_in_file)
				NumberOfImagesUsedInTraining = count_images_in_file;

#ifdef DEBUG_LEARNING
			Serial.print("ABOUT TO LEARN Image Number "); Serial.print(count_images_in_file); Serial.print(" which is OCR Image type "); Serial.print(labelNum); 
			Serial.print(" NN cat:"); Serial.print(imageCategory); Serial.print(" will be learning "); Serial.print(NUMBER_ITERATIONS_LEARN_SAME_IMAGE);
			Serial.println(" times");

			uint16_t commNeuronsPreLearn = CuriePME.getCommittedCount();
#endif
			for (int image_learn = 0; image_learn < NUMBER_ITERATIONS_LEARN_SAME_IMAGE; image_learn++)
			{
				uint16_t comm = CuriePME.learn(ProcessedImage, PROCESSED_IMAGE_SIZE, imageCategory);
#ifdef DEBUG_LEARNING
				Serial.print("Post learn committed neurons : "); Serial.println(comm);
				
				Serial.print("Image type:");
				Serial.print(labelNum);

				if (images_types[labelNum] == 1)
				{
					Serial.println(" learnt for first time");
				}
				else
				{
					Serial.println(" already know this image type");
				}

				if (comm != commNeuronsPreLearn)
				{
					//if we have learned one of these labels before the committed node should not go up -should be added to the other existing node
					Serial.print("Committed neurons is now ");
					Serial.print(comm);
					Serial.print(" went up by ");
					Serial.println(comm - commNeuronsPreLearn);
				}
				else
				{
					Serial.println("Committed neurons stayed the same!");
				}
#endif
				if (comm >= (Intel_PMT::maxNeurons - 1))
				{
#ifdef DEBUG_LEARNING
					Serial.print("!!Stop learning - Committed Neurons hit maximum the of ");
					Serial.print(comm);
					Serial.println(" neurons.");
#endif
					count_images_in_file = numberImagesToTrain;
					break;
				}
			}
		}
#ifdef DEBUG_LEARNING
		//Only print this if we are iteratively learning an image !
		if (NUMBER_ITERATIONS_LEARN_SAME_IMAGE > 1)
			Serial.println("Finished iterating learn");
#endif
		int numberOfDiffImagesWeKnow = 0;
		for (int i = 0; i< NUM_IMAGES_TYPES; i++)
		{
			if (images_types[i] > 0)
				++numberOfDiffImagesWeKnow;
		}

		if (numberOfDiffImagesWeKnow == numberOCRImageTypesToLearn)
		{
#ifdef DEBUG_LEARNING
			Serial.print("Learnt "); Serial.print(numberOfDiffImagesWeKnow); 
			Serial.println(" different types of image, reached the limit, no more new types!!");
#endif
			NoNewOCRTypes = true;
		}
	}

#ifdef DEBUG_LEARNING
	Serial.println("Learnt image types");
	for (int i = 0; i< NUM_IMAGES_TYPES; i++)
	{
		Serial.print("NN Cat:");
		Serial.print(i+1);
		Serial.print(" Learnt:");
		Serial.print(images_types[i]);
		Serial.println(" images.");
	}
#endif
	closeFiles();
	
	uint16_t newCommNeurons = CuriePME.getCommittedCount() - startCommittedNeurons;
	Serial.print("Learning phase resulted in ");
	Serial.print(newCommNeurons);
	Serial.println(" newly committed neurons.");

	NewCommittedPerTrain[NewCommittedPerTrain_Index] = newCommNeurons;
	if (NewCommittedPerTrain_Index < MAX_ITERATES_LEARN_UNTIL_NO_NEW_COMMITTED)
		++NewCommittedPerTrain_Index;
	return newCommNeurons;
}

int startNeuronIndex = 1;
int currentNeuronDisplayIndex = startNeuronIndex;

void dumpNNALL()
{
	NNInfo();
	dumpNN(MAX_NEURONS, true);
}

void printNodeData(Intel_PMT::neuronData &neuronData, bool neurons)
{
	Serial.print("\tcontext:");
	Serial.print(neuronData.context);

	Serial.print("\tinfluence:");
	Serial.print(neuronData.influence);

	Serial.print("\tminInfluence:");
	Serial.print(neuronData.minInfluence);

	Serial.print("\tcategory:");
	Serial.println(neuronData.category);

	Serial.print("\tOCR Type:");
	Serial.println(neuronData.category-1);

	if (!neurons)
		return;

	Serial.print("Vector: ");
	for (int i = 0; i < Intel_PMT::maxVectorSize; i++)
	{
		Serial.print(neuronData.vector[i]);
		if (i < (Intel_PMT::maxVectorSize-1)) 
			Serial.print(", ");
	}
	
	Serial.println("\n--------------");
}

bool checkIterator()
{
	//DONT CALL THIS IN MIDDLE OF READ OR WRITE
	if (ITERATING_NEURON_NETWORK)
	{
		Serial.println("MAJOR ERROR - TRIED TO READ NEURON - ITERATOR BEING USED ALREADY");
		return true;
	}
	return false;
}

void printNeuron(int neuron, bool eachNeuron)
{
	if (checkIterator()) return;

	Serial.print("\nNeuron #"); Serial.print(neuron);
	
	Intel_PMT::neuronData nData;

	CuriePME.readNeuron(neuron, nData);

	printNodeData(nData, eachNeuron);
}

void dumpNN(int num, bool eachNeuron)
{
	if (num > MAX_NEURONS)
		num = MAX_NEURONS;
	
	int neuronsCommitted = CuriePME.getCommittedCount();
	
	Serial.print("\nCommitted Neurons = "); Serial.println(neuronsCommitted);
	if (num == 0 || neuronsCommitted == 0)
		return;

	Serial.print("Printing :"); Serial.print(num); Serial.println(" neurons");

	for (int i = 0; i<num; i++)
	{
		printNeuron(i, eachNeuron);
	}
}

void classifyTrainDataNN()
{
	unsigned long stTime = now();
	openTrainingDataFiles();
	CurrentTestingDataState = Training;
	classifyNN();
	printResults();
	printTimeDifference("Testing with training data", stTime);
}

void classifyTestDataNN()
{
	unsigned long stTime = now();
	openTestingDataFiles();
	CurrentTestingDataState = Test;
	classifyNN();
	printResults();
	printTimeDifference("Testing with proper test data", stTime);
}

void clearArray(int *ar, int size)
{
	for (int i = 0; i<size; i++)
	{
		ar[i] = 0;
	}
}

void clearImagesArray(int *ar)
{
	clearArray(ar, NUM_IMAGES_TYPES);
}

bool intArrayContains(int *array, int size, int toFind)
{
	for (int i = 0; i < size; i++)
	{
		if (array[i] == toFind)
			return true;
	}
	return false;
}

void addTestResult(int lc, int lt, Intel_PMT::PATTERN_MATCHING_DISTANCE_MODE distMode, Intel_PMT::PATTERN_MATCHING_CLASSIFICATION_MODE clMode)
{
	Serial.print("New test result added array number ");
	Serial.println(CurrentRESULTIndex);

	RESULTS[CurrentRESULTIndex].setResult(lc, lt, distMode, clMode);
	
	CurrentRESULTIndex++;

	if ((CurrentRESULTIndex + 1) == NUMBER_PREVIOUS_RESULTS)
	{
		WrappedAroundResults = true;
		CurrentRESULTIndex = 0;
	}
}

//Testing the NN
void classifyNN()
{
	if (CurrentTestingDataState == Training)
	{
		Serial.println("Testing the network with the training data.");
		printTestOnlyWithTrainingImages();
	}
	else
	{
		Serial.println("Testing the network with the proper test data.");
	}

	NNInfo();

	int count_images = 1;
	int images_correct[NUM_IMAGES_TYPES];
	int images_incorrect[NUM_IMAGES_TYPES];
	int images_unknown[NUM_IMAGES_TYPES];
	int images_rejected[NUM_IMAGES_TYPES];
	int images_tested[NUM_IMAGES_TYPES];
	int skipped_dont_know_cat[NUM_IMAGES_TYPES];

	clearImagesArray(images_unknown);
	clearImagesArray(images_incorrect);
	clearImagesArray(images_correct);
	clearImagesArray(images_tested);
	clearImagesArray(images_rejected);
	clearImagesArray(skipped_dont_know_cat);

	if (!labelFile || !dataFile)
	{
		Serial.println("Testing failed - files not opened.");
		closeFiles();
		return;
	}

	if (labelFile->available() == 0)
	{
		Serial.println("Testing failed - label file NA");
		closeFiles();
		return;
	}

	if (dataFile->available() == 0)
	{
		Serial.println("Testing failed - data file NA");
		closeFiles();
		return;
	}

	int labelNum = 0;
	labelFile->seek(trainLabelSeekPos);
	dataFile->seek(trainDataSeekPos);

	Serial.print("Testing the first ");
	Serial.print(NUM_TO_TEST);
	Serial.println(" images");

	//Which cats does the NN know ?
	whatCatsDoesNNKnow(true);
	if (RESTRICT_TESTING_TO_KNOWN_CATS)
	{
		Serial.println("\nRestricted testing images to cats NN knows.");
	}

	printTestOnlyWithTrainingImages();

	while (count_images < NUM_TO_TEST)
	{
		labelNum = labelFile->read();
#ifdef DEBUG_CLASSIFY
		Serial.print("\n\nTesting data of OCR Type (label):");
		Serial.println(labelNum);
    Serial.print("\n\nTesting image data of a Cat:");
    Serial.println(labelNum+1);
#endif

		//If we only support testing known category types, and we dont know this type
		if (RESTRICT_TESTING_TO_KNOWN_CATS == true && intArrayContains(knownCats, NUM_IMAGES_TYPES, labelNum + 1) == false)
		{
			++skipped_dont_know_cat[labelNum];
			Serial.print("Skipped classify this image-cat:"); 
			Serial.print(labelNum + 1);
			Serial.print(" OCR type:");
			Serial.print(labelNum);
			Serial.println(" NN ignorant of this cat");
		}
		else
		{
			//reads OCR data from file 
			for (int i = 0; i < LEN; i++)
			{
				RawImage[i] = dataFile->read();
			}

			//Prep the image
			processImage();

			int NNanswer = INITIAL_CATEGORY;
#ifdef DEBUG_CLASSIFY
			Serial.print("Testing Image #");
			Serial.print(count_images);
			Serial.print("-OCR Type (label):");
			Serial.print(labelNum);
			Serial.print(" Told NN the type is (cat):");
			Serial.println(labelNum + 1);
#endif
			NNanswer = CuriePME.classify(ProcessedImage, PROCESSED_IMAGE_SIZE);
      Serial.print("The NN classified the image as a cat (answer):"); Serial.println(NNanswer);

			++images_tested[labelNum];

			if (NNanswer != Intel_PMT::noMatch)
			{
				if (NNanswer == INITIAL_CATEGORY)
				{
					++images_unknown[labelNum];
					Serial.println("NN incorrect cat-same as 99 initial cat");
				}
				else
				{
					if (NNanswer - 1 == labelNum)
					{
						Serial.print("NN CORRECT: ");
						images_correct[labelNum] = images_correct[labelNum] + 1;
					}
					else
					{
						Serial.print("NN INCORRECT !! NN thought it was cat: ");
						Serial.print(NNanswer);
						images_incorrect[labelNum] = images_incorrect[labelNum] + 1;
					}
         
          Serial.print(" NN guess was OCR type:");
          Serial.println(NNanswer - 1);

          Serial.print(images_correct[labelNum]);
          Serial.print(" images so far correct and ");

          Serial.print(images_incorrect[labelNum]);
          Serial.print(" images so far incorrect of image type ");

          Serial.println(labelNum);

					Serial.print("Now we have tested a total of ");
					Serial.print(count_images);
         Serial.println(" images.");
				}
			}
			else
			{
				//The NN came back with no match
				Serial.print("NN came back with 0x7FFF - nomatch. Total of ");
				images_rejected[labelNum] = images_rejected[labelNum]+ 1;
        Serial.print(images_rejected[labelNum]);
        Serial.println(" with no match so far.");
			}

			Serial.print("Finished testing image #");
			Serial.println(count_images);
			if (CurrentTestingDataState == Training && UseOnlyImagesFromTraining == true && count_images >= NumberOfImagesUsedInTraining)
			{
				Serial.println("Stopped testing as we have tested all the imges that were used in training.");
				break;
			}
			count_images++;
		}
	}

	Serial.print("\n\nFinished, we tested ");
	Serial.print(count_images);
	Serial.println(" images in total.");

	int correct = 0;

	for (int index = 0; index<NUM_IMAGES_TYPES; index++)
	{
		Serial.print("Image label ");
		Serial.print(index);
		Serial.print(" we tested ");
		Serial.print(images_tested[index]);
		Serial.print(" and classified ");
		Serial.print(images_correct[index]);
		Serial.print(" correct, and ");
		Serial.print(images_incorrect[index]);
		Serial.print(" incorrect and ");
		Serial.print(images_unknown[index]);
		Serial.print(" unknown images, and ");
		Serial.print(images_rejected[index]);
		Serial.println(" rejected with (0x7FFF).");
		correct += images_correct[index];
	}

	if (RESTRICT_TESTING_TO_KNOWN_CATS)
	{
		Serial.println("Restricted testing to known cats is on");
		for (int index = 0; index < NUM_IMAGES_TYPES; index++)
		{
			if (skipped_dont_know_cat[index] > 0)
			{
				Serial.print("Skipped ");
				Serial.print(skipped_dont_know_cat[index]);
				Serial.print(" images of category:");
				Serial.println(index);
			}
		}
	}

	Serial.print("\n\nNN got :");
	Serial.print(correct);
	Serial.print(" correct out of ");
	Serial.print(count_images);
	Serial.println(" images tested");

	Serial.println("Adding test result to historical results.");
	printNNMode();
	addTestResult(correct, count_images, CuriePME.getDistanceMode(), CuriePME.getClassifierMode());

	closeFiles();
}

void logUserOption(char g)
{
	COMMANDS_LIST[nextCommand] = g;
	nextCommand++;

	//wrap around
	if (nextCommand == MAX_COMMANDS_HISTORY_LEN)
		nextCommand = 0;
}

void printCommandList()
{
	Serial.print("Previous user commands: ");
	for (int i = 0; i< MAX_COMMANDS_HISTORY_LEN; ++i)
	{
		Serial.print(COMMANDS_LIST[i]); Serial.print(" ,");
	}
}

void setGContext(uint16_t gc)
{
	Serial.print("Attempting to set globalcontext to:");
	Serial.println(gc);

	if (isValidcontext(gc) == false)
		return;

	CuriePME.setGlobalContext(gc);

	uint16_t newGC = CuriePME.getGlobalContext();
	
	if (newGC != gc)
	{
		Serial.print("Failed to ");
	}

	Serial.print("set Global Context to:");
	Serial.println(gc);
	Serial.print("Global context is Currently:");
	Serial.println(newGC);
}

void toggleLearnUntilNNCN()
{
	if (CuriePME.getClassifierMode() == Intel_PMT::PATTERN_MATCHING_CLASSIFICATION_MODE::KNN_Mode)
	{
		Serial.print("KNN Mode doesnt support more committed neurons per test run - should be one per cat ");
		Serial.println(" -no point in training until new committed = 0");
		return;
	}

	printIterationStatus();
	if (ITERATE_LEARN_UNTIL_NO_NEW_COMMITTED)
		ITERATE_LEARN_UNTIL_NO_NEW_COMMITTED = false;
	else
		ITERATE_LEARN_UNTIL_NO_NEW_COMMITTED = true;
	printIterationStatus();
}

char toUpper(char option)
{
	if (option >= 'a' && option <= 'z')
		option += ('a' - 'A');
	return option;
}

void printResults()
{
	//Wrap around index so print in 2 phases

	int startIndex = CurrentRESULTIndex;
	int endIndex = NUMBER_PREVIOUS_RESULTS;
	int stages = 0;

	if (WrappedAroundResults == false)
	{
		startIndex = 0;
		endIndex = CurrentRESULTIndex;
		stages = 1;
	}

	for (; stages < 2; stages++)
	{
		for (int i = startIndex; i < endIndex; i++)
		{
			RESULTS[i].print(i+1);
		}

		startIndex = 0;
		endIndex = CurrentRESULTIndex - 1;
	}
}

void doubleTestImages()
{
	NUM_TO_TEST = NUM_TO_TEST * 2;
	Serial.print("Now testing :");
	Serial.println(NUM_TO_TEST);
}

void learnAndTestAndSave(int phase)
{
	unsigned long superTestTime = now();

	Serial.print("\n\n\nSuper Test - Phase ");
	Serial.println(phase);
	Serial.println("-----------------");

	Serial.print("Network is in this state in Phase ");
	Serial.println(phase);
	NNInfo();

	Serial.print("TRAINing Phase ");
	Serial.println(phase);
	trainNN();

	Serial.print("Saving Phase ");
	Serial.println(phase);
	saveNetworkKnowledge();

	Serial.print("Testing with TRAINing data in Phase ");
	Serial.println(phase);
	classifyTrainDataNN();
	
	Serial.print("Network was in this state in Phase ");
	Serial.println(phase);
	NNInfo();

	Serial.print("Print Super Test Results Phase ");
	Serial.println(phase);
	printResults();

	String opName = "Super Test Phase " + char('0'+phase);
	
	printTimeDifference(opName, superTestTime);
}

void superTest()
{
	unsigned long superTestTime = now();

	Serial.println("Super Test - Phase 1");
	learnAndTestAndSave(1);

	toggleDistanceMode();

	Serial.println("Super Test - Phase 2");
	learnAndTestAndSave(2);

	toggleNNType();

	Serial.println("Super Test - Phase 3");
	learnAndTestAndSave(3);

	toggleDistanceMode();

	Serial.println("Super Test - Phase 4");
	learnAndTestAndSave(4);

	printTimeDifference("Whole Super Test", superTestTime);
}


void toggleTestOnlyWithTrainingImages()
{
	printTestOnlyWithTrainingImages();
	if (UseOnlyImagesFromTraining == true)
	{
		UseOnlyImagesFromTraining = false;
	}
	else
	{
		UseOnlyImagesFromTraining = true;
	}
	printTestOnlyWithTrainingImages();
}

void loopProper()
{
	Serial.println("\n\nWelcome !! Marcus Neural Network Machine");
	Serial.println(VERSION);
	Serial.println("Option");
	Serial.println("------");
	Serial.println("1. Clear NN");
	Serial.println("2. Train NN");
	Serial.println("3. Test NN with training data");
	Serial.println("4. Test NN with test data");
	Serial.println("5. View Neural Network State");
	Serial.println("6. View all neurons");
	Serial.println("7. View committed neurons");
	Serial.println("8. SD Card Info");
	Serial.println("0. Toggle NN type : KNN or RBF");
	Serial.println("Z. Toggle Distance mode L1 or LSUP");
	Serial.println("R. Toggle testing only known categories");
	Serial.println("X. Toggle testing with only known images");
	Serial.println("N. Toggle training until no new committed neruons");
	Serial.println("G. Set Global context to 1");
	Serial.println("U. Super test");
	Serial.println("P. Print out user commands");
	Serial.println("E. Print previous results");
	Serial.println("D. Double number of test images");
	Serial.println("L. Load the NN");
	Serial.println("S. Save the NN");

	Serial.println("\n\nOption:");

	while (!Serial.available()) {}

	char option = Serial.read();
	
	//Set it to upper case if its lower
	option = toUpper(option);

	Serial.print("Selected:");
	Serial.println(option);
	Serial.println("\n\n");
	logUserOption(option);
	String comm = "";

	switch (option)
	{
	case 'N':
		comm = "Toggle training until no new committed neruons";
		Serial.println(comm);
		toggleLearnUntilNNCN();
		break;
	case 'L':
		comm = "Loading";
		Serial.println(comm);
		restoreNetworkKnowledge();
		break;
	case 'D':
		comm = "Double test images";
		Serial.println(comm);
		doubleTestImages();
		break;
	case 'S':
		comm = "Saving";
		Serial.println(comm);
		saveNetworkKnowledge();
		break;
	case 'U':
		//start with lsup and KNN
		comm = "Super Test!";
		Serial.println(comm);
		superTest();		
		break;
	case '1':
		comm = "Reset NN";
		Serial.println(comm); 
		clearNN();
		break;
	case '2':
		comm = "Training";
		Serial.println(comm); 
		trainNN();
		break;
	case '3':
		comm = "Testing with training data";
		Serial.println(comm); 
		classifyTrainDataNN();
		break;
	case '4':
		comm = "Testing with test data";
		Serial.println(comm); 
		classifyTestDataNN();
		break;
	case '5':
		comm = "View Network";
		Serial.println(comm); 
		NNInfo();
		break;
	case '6':
		comm = "Print out All Neurons";
		Serial.println(comm); 
		dumpNNALL();
		break;
	case '7':
		comm = "Print Committed neurons";
		Serial.println(comm); 
		dumpNN(CuriePME.getCommittedCount(), true);
		break;
	case '8':
		comm = "Check the SD";
		Serial.println(comm); 
		checkSDCard();
		break;
	/*case '9':
		Serial.println("Check board status - BLINK !");
		blink();
		break;*/
	case 'G':
		comm = "Set Global context to " + NETWORK_CONTEXT;
		if (isValidcontext(NETWORK_CONTEXT) == false)
			break;

		Serial.println(comm);
		setGContext(1);
		break;
	case 'R':
		comm = "Toggling testing only known categories";
		Serial.println(comm);
		toggleRestrictTesting();
		break;
	case 'X':
		comm = "Toggling test only with exactly the same training images";
		Serial.println(comm);
		toggleTestOnlyWithTrainingImages();
		break;
	case '0':
		comm = "Toggling the NN type";
		Serial.println(comm); 
		toggleNNType();
		break;
	case 'Z':
		comm = "Toggling the Distance Mode";
		Serial.println(comm); 
		toggleDistanceMode();
		break;
	case 'P':
		comm = "Print commands";
		Serial.println(comm); 
		printCommandList();
		break;
	case 'E':
		comm = "Print previous results";
		Serial.println(comm);
		printResults();
		break;
	default:
		Serial.println("Invalid Option.");
		return;
	Serial.println("Finished " + comm);
	}
}

const char* getFileName()
{
	const char *filename = "NEURDATA.DAT";
	if (CuriePME.getClassifierMode() == Intel_PMT::PATTERN_MATCHING_CLASSIFICATION_MODE::KNN_Mode)
		filename = "NEURDKNN.DAT";
	return filename;
}

void saveNetworkKnowledge()
{
	if (checkIterator()) return;

	const char *filename = getFileName();

	Serial.print("Saving to file:");
	Serial.println(filename);

	if (SD.exists(filename) == true)
	{
		Serial.println("File exists so delete it !");
		SD.remove(filename);
		if (SD.exists(filename) == true)
		{
			Serial.println("FAILED to save knowledge - File exists could not delete it !");
			return;
		}
	}

	// Open the file and write test data
	File *file = new File(SD.open(filename, FILE_WRITE));

	if (file)
	{
		Intel_PMT::neuronData neuronData;
		CuriePME.endSaveMode();
		CuriePME.endRestoreMode();

		Serial.print("There are "); Serial.print(CuriePME.getCommittedCount()); Serial.println(" committed neurons");

		ITERATING_NEURON_NETWORK = true;

		CuriePME.beginSaveMode();
		int count = 1;
		while (uint16_t nCount = CuriePME.iterateNeuronsToSave(neuronData))
		{
			if (nCount == Intel_PMT::noMatch)
			{
				Serial.println("Hit 0x7FFF - nomatch");
				break;
			}
			else
			{
				Serial.print("Saving node#:"); 
				Serial.print(count); 
				Serial.print(" with cat #"); 
				Serial.print(nCount);
				Serial.print(" OCR type:");
				Serial.print(nCount - 1);

				uint16_t neuronFields[4];

				neuronFields[0] = neuronData.context;
				neuronFields[1] = neuronData.influence;
				neuronFields[2] = neuronData.minInfluence;
				neuronFields[3] = neuronData.category;
	
				file->write((uint8_t*)neuronFields, 8);
				file->write((uint8_t*)neuronData.vector, Intel_PMT::maxVectorSize);

				printNodeData(neuronData, true);
				
				Serial.print("Saved Cat Neuron:"); 
				Serial.println(nCount);

				Serial.print("Saved Neuron#");
				Serial.println(count);
				++count;
			}
		}
		ITERATING_NEURON_NETWORK = false;

		file->write((uint8_t*)"EOF!", 4);
		file->close();
		Serial.println("File closed");
	}
	else
	{
		Serial.print("Problem opening file to save NN to ");
		Serial.println(filename);
	}

	CuriePME.endSaveMode();
}

bool isValidcontext(uint16_t context)
{
	if (context < Intel_PMT::minContext +1 || context > Intel_PMT::maxContext)
	{
		Serial.print("The context ");
		Serial.print(context);
		Serial.print(" was not between ");
		Serial.print(Intel_PMT::minContext);
		Serial.print(" and ");
		Serial.print(Intel_PMT::maxContext);
		Serial.print(" so quit loop in read from file");
		return false;
	}
	return true;
}

void restoreNetworkKnowledge()
{
	if (checkIterator()) return;

	const char *filename = getFileName();

	File *file = new File(SD.open(filename));
	if (!file)
	{
		Serial.print("Failed to open the NN data file:");
		Serial.println(filename);
		file->close();
		return;
	}
	else
	{
		Serial.print("Opened the NN data file:");
		Serial.println(filename);

		if (file->available())
		{
			Serial.println("NN data file available.");
		}
		else
		{
			Serial.println("NN Data file NOT available.");
			file->close();
			return;
		}
	}

	int32_t fileNeuronCount = 1;

	ITERATING_NEURON_NETWORK = true;

	CuriePME.beginRestoreMode();
	Intel_PMT::neuronData neuronData;

	//Dont trust this
	while (1)
	{
		int posit = file->position();
		uint16_t neuronFields[4];
		char testData[4];

		file->read((char*)testData, 4);

		if (testData[0] == 'E' &&
			testData[1] == 'O' &&
			testData[2] == 'F' &&
			testData[3] == '!')
		{
			Serial.println("EOF!");
			break;
		}

		file->seek(posit);

		Serial.print("Reading Neuron:");
		Serial.println(fileNeuronCount);

		file->read((uint8_t*)neuronFields, 8);
		if (isValidcontext(neuronFields[0]) == false)
			break;

		file->read((uint8_t*)neuronData.vector, Intel_PMT::maxVectorSize);

		neuronData.context = neuronFields[0];
		neuronData.influence = neuronFields[1];
		neuronData.minInfluence = neuronFields[2];
		neuronData.category = neuronFields[3];

		Serial.println("Applied Neuron#");
		Serial.print(fileNeuronCount);
		
		fileNeuronCount++;

		printNodeData(neuronData, true);

		CuriePME.iterateNeuronsToRestore(neuronData);
	}
	
	ITERATING_NEURON_NETWORK = false;

	CuriePME.endRestoreMode();
	Serial.println("Restored NN");
}
