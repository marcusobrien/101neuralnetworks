# README #

This README is a quick description on the MNIST pattern recogniser I wrote for the Intel Arduino 101.

### What is this repository for? ###

* You can download this code to the 101 and use the Serial Monitor to drive the menus to control the 101 hardware neural network
* Version 1.0

### How do I get set up? ###

This is really simple. You just copy the code from here, and using the Arduino IDE download it to your Intel 101. Then run it, connect a serial Monitor and use the text based menus.

### Contribution guidelines ###

Send any improvements or suggestions to me - Marcus O'Brien

### Who do I talk to? ###

Marcus O'Brien